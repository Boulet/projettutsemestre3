﻿using System;

namespace Tab3d
{
    class Program
    {
    static void Main(string[] args)
        {
            int x = 10;
            int y = 10;
            int frontieres = 4;
            // Frontiere 1 : Frontiere nord
            // Frontiere 2 : Frontiere est
            // Frontiere 3 : Frontiere sud
            // Frontiere 4 : Frontiere ouest
            bool[,,] tab = new bool[x, y, frontieres];
            tab[1,1,1] = true; //Ici nous disons que la case [1,1] à une frontiere au nord
            tab[1,1,2] = false; //Ici nous disons que la case [1,1] n'a pas de frontière à l'est 

            Console.WriteLine(tab[1,1,1]); 
            Console.WriteLine(tab[1,1,2]);

            Console.ReadKey();
        }
    }
}
