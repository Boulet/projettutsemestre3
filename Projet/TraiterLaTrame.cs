using System;

namespace Projet
{
    partial class Program
    {   public static Carte determine_quelles_cases_sont_jouables(Carte map){
            Carte map_cases_jouables = new Carte();
            map_cases_jouables = map;
            for (int indice_ligne = 0; indice_ligne < 10 ; ++indice_ligne){
                for (int indice_colonne = 0; indice_colonne < 10 ; ++indice_colonne){
                    if(map.get_tab_cellules(indice_ligne,indice_colonne).get_valeur() >= 32){
                        map_cases_jouables.get_tab_cellules(indice_ligne,indice_colonne).set_est_une_case_jouable(false);
                    }else{
                        map_cases_jouables.get_tab_cellules(indice_ligne,indice_colonne).set_est_une_case_jouable(true); 
                    }
                }
            }
            return map_cases_jouables;
        }

        public static Carte decouper_la_trame(string chaine){
            string[] ligne = new string[10];
            string[,] map = new string [10,10];
            string[] detail_ligne = new string[10];
            ligne = chaine.Split('|');
            Carte map_x_y = new Carte();
            for (int i = 0 ; i<10 ; i++ ){
                detail_ligne = ligne[i].Split(':'); 
                for (int j = 0 ; j<10 ; j++){
                     map_x_y.get_tab_cellules(i,j).set_valeur(int.Parse(detail_ligne[j]));
                }
            }
            return map_x_y;
        }

        public static bool[] y_a_t_il_une_frontiere(int valeur_cellule){
            int la_valeur = valeur_cellule;
            bool[] tableau_frontieres = new bool[4];
            if (la_valeur - 64 >=0){
                la_valeur -= 64;
            }
            if (la_valeur - 32>=0){
                la_valeur -=32;
            }
            if (la_valeur - 8 >= 0){    // EST
                la_valeur -= 8;
                tableau_frontieres[0] = true;
            }else{tableau_frontieres[0] = false;}

            if (la_valeur - 4 >= 0){    //SUD
                la_valeur -= 4;
                tableau_frontieres[1] = true;
            }else{tableau_frontieres[1] = false;}  

            if (la_valeur - 2 >= 0){    // OUEST
                la_valeur -= 2;
                tableau_frontieres[2] = true;
            }else{tableau_frontieres[2] = false;}

            if (la_valeur - 1 >= 0){    //NORD
                la_valeur -= 1;
                tableau_frontieres[3] = true;
            }else{tableau_frontieres[3] = false;}
            return tableau_frontieres;
        }
        
        
        public static Carte attribution_des_frontieres(Carte map){
            Carte map_frontiere = new Carte();
            map_frontiere = map;
            bool[] tableau_frontieres = new bool [4];
            
            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++){
                for (int indice_colonne = 0; indice_colonne < 10; indice_colonne++){
                    tableau_frontieres = y_a_t_il_une_frontiere(map_frontiere.get_tab_cellules(indice_ligne,indice_colonne).get_valeur());
                    map_frontiere.get_tab_cellules(indice_ligne,indice_colonne).set_frontiere_est(tableau_frontieres[0]);
                    map_frontiere.get_tab_cellules(indice_ligne,indice_colonne).set_frontiere_sud(tableau_frontieres[1]);
                    map_frontiere.get_tab_cellules(indice_ligne,indice_colonne).set_frontiere_ouest(tableau_frontieres[2]);
                    map_frontiere.get_tab_cellules(indice_ligne,indice_colonne).set_frontiere_nord(tableau_frontieres[3]);         
                }
            }
            return map_frontiere;
        }
        /* FONCTION "Main" de traitement */
        static Carte traitement_de_la_trame(string trame)
        {
            string[,] string_map = new string [10,10];
            Carte map = new Carte();
            map = decouper_la_trame(trame); //Ici On appel la fonction decouper_la_carte 
            map = determine_quelles_cases_sont_jouables(map); /*Ici cette fonction est pour le moment inutile*/
            map = attribution_des_frontieres(map);
            return map;
        }
    }
}