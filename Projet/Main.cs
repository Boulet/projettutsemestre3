﻿using System;

namespace Projet
{
    partial class Program
    {   
        static void Main(string[] args)
        {
            Console.WriteLine("Début de Main !");   Console.WriteLine("");

            string trame;
            Ia IA = new Ia();
            
            Console.WriteLine("Tentative de connexion");
            connexion();
            Console.WriteLine("On est connecté");   Console.WriteLine("");

            Console.WriteLine("Appel de la fonction recuperer_la_trame");
            trame = recuperer_la_trame();
            Console.WriteLine("Retour dans le main");   Console.WriteLine("");

            Carte map = new Carte();

            Console.WriteLine("Appel de la fonction traitement_de_la_trame");
            map = traitement_de_la_trame(trame);
            Console.WriteLine("Retour dans le main");   Console.WriteLine("");

            Console.WriteLine("Appel de la fonction ranger_les_cellules_dans_des_parcelles");
            map = ranger_les_cellules_dans_des_parcelles(map);
            Console.WriteLine("Retour dans le main");

            Console.WriteLine("Debut de la recherch des tailles");
            map.determiner_la_taille_des_parcelles();
            int[] liste_tailles = map.get_tailles_parcelles();
            for(int i=0; i<17;i++){
                Console.WriteLine(liste_tailles[i]);
            }

            Console.WriteLine("affichage_de_la_carte");
            map.afficher_la_carte();
            Console.WriteLine("Retour dans le main");

            string chaine ="A:11";
            Console.WriteLine("Tentative d'envoie de données");
            envoie_de_donnees(chaine);
            Console.WriteLine("Fin de la tentative"); Console.WriteLine("");

            
            bool jeuFini=false;

            while(!jeuFini){
                Console.WriteLine("Appel de la fonction recuperer_la_trame");
                String reponse = recuperer_la_reponse();
                Console.WriteLine("Retour dans le while");   Console.WriteLine("");
                
                Console.WriteLine("Debut de la demande de validation_du_coup");
                validation_du_coup(reponse);
                Console.WriteLine("Fin de la demande"); Console.WriteLine("");

                Console.WriteLine("Appel de la fonction recuperer_la_reponse");
                reponse = recuperer_la_reponse();
                Console.WriteLine("Retour dans le while");   Console.WriteLine("");
                jeuFini = tour_ennemie(reponse);
                
                if(!jeuFini){
                    
                    Console.WriteLine("Appel de la fonction recuperer_la_reponse");
                    reponse = recuperer_la_reponse();
                    Console.WriteLine("Retour dans le if");   Console.WriteLine("");
                    Console.WriteLine("Debut de fin de tour");
                    jeuFini = fin_du_tour_ennemie(reponse);
                    Console.WriteLine("fin de fin de tour");
                    if(jeuFini){
                        Console.WriteLine("recup score");
                        recuperer_le_score();
                        Console.WriteLine("on a le score");
                    }else{
                        Console.WriteLine("Ia JOUE");
                        IA.joue(map);
                        Console.WriteLine("ia a joué");
                        envoie_de_donnees(IA.get_jeu_IA());
                        Console.WriteLine("donnee envoyees");
                    }
                }else{
                    recuperer_le_score();
                }
                
            }

            Console.WriteLine("Deconnexion");
            deconnexion();
            Console.WriteLine("On est deconnecté ! ");  Console.WriteLine("");

            Console.WriteLine("Fin de main");
        }
    }
}
