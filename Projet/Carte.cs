using System;

namespace Projet
{
    class Carte
    {

        protected Cellule[,] tab_cellules = new Cellule[10,10];
        protected Parcelle[] tab_parcelles = new Parcelle[17];

        protected int[] liste_tailles; 

        public Carte()
        {
            for (int indice_ligne = 0; indice_ligne < 10; indice_ligne++){
                for (int indice_colone = 0; indice_colone <10; indice_colone++) {
                    tab_cellules[indice_ligne,indice_colone] = new Cellule(indice_ligne,indice_colone);
                }
            }
            for (int indice_parcelle = 0; indice_parcelle < 17; indice_parcelle++) {
                tab_parcelles[indice_parcelle] = new Parcelle(); 
            }
        }

        //Getter
        public Cellule get_tab_cellules(int x, int y)
        {
            return tab_cellules[x,y];
        }
        public Parcelle get_parcelle(int numero_de_parcelle)
        {
            return tab_parcelles[numero_de_parcelle];
        }

        public Parcelle[] get_parcelles(){
            return this.tab_parcelles;
        }

        public int[] get_tailles_parcelles(){
            return this.liste_tailles;
        }

        
        public void determiner_la_taille_des_parcelles(){
            Parcelle[] tab_parcelles = this.get_parcelles();
            int[] liste_tailles = new int[17];
            for(int i=0; i<17;i++){
                liste_tailles[i]=tab_parcelles[i].get_taille();
            }
        }

        public void afficher_la_carte(){
            String ma_ligne = "";
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    switch (this.get_tab_cellules(i, j).get_numero_de_parcelle_de_la_cellule()) // on récupere le numéro de parcelle
                    {
                        case 0:
                            ma_ligne = ma_ligne + "a  ";
                            break;
                        case 1:
                            ma_ligne = ma_ligne + "b  ";
                            break;
                        case 2:
                            ma_ligne = ma_ligne + "c  ";
                            break;
                        case 3:
                            ma_ligne = ma_ligne + "d  ";
                            break;
                        case 4:
                            ma_ligne = ma_ligne + "e  ";
                            break;
                        case 5:
                            ma_ligne = ma_ligne + "f  ";
                            break;
                        case 6:
                            ma_ligne = ma_ligne + "g  ";
                            break;
                        case 7:
                            ma_ligne = ma_ligne + "h  ";
                            break;
                        case 8:
                            ma_ligne = ma_ligne + "i  ";
                            break;
                        case 9:
                            ma_ligne = ma_ligne + "j  ";
                            break;
                        case 10:
                            ma_ligne = ma_ligne + "k  ";
                            break;
                        case 11:
                            ma_ligne = ma_ligne + "l  ";
                            break;
                        case 12:
                            ma_ligne = ma_ligne + "m  ";
                            break;
                        case 13:
                            ma_ligne = ma_ligne + "n  ";
                            break;
                        case 14:
                            ma_ligne = ma_ligne + "o  ";
                            break;
                        case 15:
                            ma_ligne = ma_ligne + "p  ";
                            break;
                        case 16:
                            ma_ligne = ma_ligne + "q  ";
                            break;
                        case -1:
                            int valeur = this.get_tab_cellules(i, j).get_valeur();
                            if (valeur >= 64)
                            {
                                ma_ligne = ma_ligne + "M  "; // Symbolysation d'une case Mer
                            }
                            else
                            {
                                ma_ligne = ma_ligne + "F  ";// Symbolisation d'une case For�t
                            }
                            
                            break;
                    }
                }
                Console.WriteLine(ma_ligne);
                ma_ligne = "";
            }
        }
        
    }
}
