using System;

namespace Projet
{
    public class Cellule
    {
        protected int x; // A placer en private
        protected int y;
        protected int valeur; //Ici la valeur est la valeur qu'on � trouv� depuis la trame
        protected int numero_de_parcelle_de_la_cellule;
        protected bool frontiere_est;
        protected bool frontiere_sud;
        protected bool frontiere_ouest;
        protected bool frontiere_nord;
        protected bool est_une_case_jouable;
        protected bool appartient_a_une_parcelle;

        public Cellule() 
        {
            this.x = 0;
            this.y = 0;
            this.valeur = 0;
            this.numero_de_parcelle_de_la_cellule = -1; // ceci n'est pas propre, à corriger !
            this.frontiere_est = false;
            this.frontiere_sud = false;
            this.frontiere_ouest = false;
            this.frontiere_nord = false;
            this.appartient_a_une_parcelle = false;
        }
        public Cellule(int x, int y)
        {
            this.x = x;
            this.y = y;
            this.valeur = 0;
            this.numero_de_parcelle_de_la_cellule = -1; // ceci n'est pas propre, à corriger ! 
            this.frontiere_est = false;
            this.frontiere_sud = false;
            this.frontiere_ouest = false;
            this.frontiere_nord = false;
            this.appartient_a_une_parcelle = false;
        }


        // Getter
        public int get_x()
        {
            return this.x;
        }
        public int get_y()
        {
            return this.y;
        }
        public int get_valeur()
        {
            return this.valeur;
        }
        public int get_numero_de_parcelle_de_la_cellule()
        {
            return this.numero_de_parcelle_de_la_cellule;
        }
        public bool get_frontiere_est()
        {
            return this.frontiere_est;
        }
        public bool get_frontiere_sud()
        {
            return this.frontiere_sud;
        }
        public bool get_frontiere_ouest()
        {
            return this.frontiere_ouest;
        }
        public bool get_frontiere_nord()
        {
            return this.frontiere_nord;
        }
        public bool get_est_une_case_jouable()
        {
            return this.est_une_case_jouable;
        }
        public bool get_appartient_a_une_parcelle()
        {
            return this.appartient_a_une_parcelle;
        }


        //setter
        public void set_x(int x)
        {
            this.x = x;
        }
        public void set_y(int y)
        {
            this.y = y;
        }
        public void set_valeur(int valeur)
        {
            this.valeur = valeur;
        }
        public void set_numero_de_parcelle_de_la_cellule(int numero_de_parcelle_de_la_cellule)
        {
            this.numero_de_parcelle_de_la_cellule = numero_de_parcelle_de_la_cellule;
        }
        public void set_frontiere_est(bool frontiere_est)
        {
            this.frontiere_est = frontiere_est;
        }
        public void set_frontiere_sud(bool frontiere_sud)
        {
            this.frontiere_sud = frontiere_sud;
        }
        public void set_frontiere_ouest(bool frontiere_ouest)
        {
            this.frontiere_ouest = frontiere_ouest;
        }
        public void set_frontiere_nord(bool frontiere_nord)
        {
            this.frontiere_nord = frontiere_nord;
        }
        public void set_est_une_case_jouable(bool est_une_case_jouable)
        {
            this.est_une_case_jouable = est_une_case_jouable;
        }
        public void set_appartient_a_une_parcelle(bool appartient_a_une_parcelle)
        {
            this.appartient_a_une_parcelle = appartient_a_une_parcelle;
        }
    }
    
}