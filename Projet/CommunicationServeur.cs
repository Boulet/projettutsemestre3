using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace Projet
{
    partial class Program
    {   
        static Socket socketClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        public static void connexion(){
            Encoding unicode = Encoding.Unicode;      
            try
            {
                socketClient.Connect("127.0.0.1", 1213);
            }
            catch(Exception Erreur)
            {
                Console.WriteLine(Erreur.ToString());
                Console.ReadKey();
            }
        }

        public static void deconnexion(){
            socketClient.Shutdown(SocketShutdown.Receive);
            socketClient.Close();
        }
        public static string recuperer_la_trame()
        {
            string chaine;
            int reception = 0;
            byte[] donnees = new byte[259];
            reception = socketClient.Receive(donnees);
            chaine = Encoding.ASCII.GetString(donnees);
            Console.WriteLine(chaine); // On affiche un peu la chaine 
            
            return chaine;
        }

        public static string recuperer_la_reponse()
        {
            string chaine;
            int reception = 0;
            byte[] donnees = new byte[4];
            reception = socketClient.Receive(donnees);
            chaine = Encoding.ASCII.GetString(donnees);
            Console.WriteLine(chaine); // On affiche un peu la chaine 
            
            return chaine;
        }

        public static string recuperer_le_score()
        {
            string chaine;
            int reception = 0;
            byte[] donnees = new byte[7];
            reception = socketClient.Receive(donnees);
            chaine = Encoding.ASCII.GetString(donnees);
            Console.WriteLine(chaine); // On affiche un peu la chaine 
            
            return chaine;
        }

        public static void envoie_de_donnees(string chaine){
            byte[] donnees = new byte[4];
            donnees = Encoding.ASCII.GetBytes(chaine);
            socketClient.Send(donnees);
        }
    }
}