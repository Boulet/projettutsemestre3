using System;

namespace Projet

{
    
    partial class Program
    {
        static int cellule_courrante = 1; //Les variables comme celle-ci c'est pas tr�s beau 

        //Cette fonction regarde � l'EST de la cellule
        static Carte on_regarde_en_est(Carte map_recue, Cellule la_cellule, int numero_de_parcelle)
        {
            Carte map_avec_copines = new Carte();
            map_avec_copines = map_recue;
            Cellule ma_cellule = new Cellule();
            ma_cellule = la_cellule;
            int parcelle_courrante = numero_de_parcelle;

            if (ma_cellule.get_y() < 9)
            {
                if (map_avec_copines.get_tab_cellules(ma_cellule.get_x(), (ma_cellule.get_y() + 1)).get_est_une_case_jouable() & !(map_avec_copines.get_tab_cellules(ma_cellule.get_x(), (ma_cellule.get_y() + 1)).get_appartient_a_une_parcelle()) & !(ma_cellule.get_frontiere_est()))
                {
                    map_avec_copines.get_tab_cellules(ma_cellule.get_x(), (ma_cellule.get_y() + 1)).set_numero_de_parcelle_de_la_cellule(parcelle_courrante); //Maintenant sa variable numparcelledelacellule vaut parcelle courrante
                    map_avec_copines.get_tab_cellules(ma_cellule.get_x(), (ma_cellule.get_y() + 1)).set_appartient_a_une_parcelle(true);//Maintenant elle appartient a une parcelle
                    map_avec_copines.get_parcelle(parcelle_courrante).set_cellule(map_avec_copines.get_tab_cellules(ma_cellule.get_x(), (ma_cellule.get_y() + 1)), cellule_courrante);//Maintenant elle est dans la parcelle courrante
                    cellule_courrante++;
                    map_avec_copines = on_regarde_autour(map_avec_copines, map_avec_copines.get_tab_cellules(ma_cellule.get_x(), (ma_cellule.get_y() + 1)), parcelle_courrante);
                    
                }
            }
            return map_avec_copines;
        }
        //Cette fonction regarde au SUD de la cellule
        static Carte on_regarde_au_sud(Carte map_recue, Cellule la_cellule, int numero_de_parcelle)
        {
            Carte map_avec_copines = new Carte();
            map_avec_copines = map_recue;
            Cellule ma_cellule = new Cellule();
            ma_cellule = la_cellule;
            int parcelle_courrante = numero_de_parcelle;
            if (ma_cellule.get_x() < 9)
            {
                if (map_avec_copines.get_tab_cellules((ma_cellule.get_x() + 1), ma_cellule.get_y()).get_est_une_case_jouable() & !(map_avec_copines.get_tab_cellules((ma_cellule.get_x() + 1), ma_cellule.get_y()).get_appartient_a_une_parcelle()) & !(ma_cellule.get_frontiere_sud()))
                {                    
                    map_avec_copines.get_tab_cellules((ma_cellule.get_x() + 1), ma_cellule.get_y()).set_numero_de_parcelle_de_la_cellule(parcelle_courrante); //Maintenant sa variable numparcelledelacellule vaut parcelle courrante
                    map_avec_copines.get_tab_cellules((ma_cellule.get_x() + 1), ma_cellule.get_y()).set_appartient_a_une_parcelle(true);//Maintenant elle appartient a une parcelle
                    map_avec_copines.get_parcelle(parcelle_courrante).set_cellule(map_avec_copines.get_tab_cellules((ma_cellule.get_x() + 1), ma_cellule.get_y()), cellule_courrante);//Maintenant elle est dans la parcelle courrante
                    cellule_courrante++; 
                    map_avec_copines = on_regarde_autour(map_avec_copines, map_avec_copines.get_tab_cellules((ma_cellule.get_x() + 1), ma_cellule.get_y()), parcelle_courrante);                  
                    
                }
            }
            return map_avec_copines;
        }
        //Cette fonction regarde � l'OUEST de la cellule
        static Carte on_regarde_en_ouest(Carte map_recue, Cellule la_cellule, int numero_de_parcelle)
        {
            Carte map_avec_copines = new Carte();
            map_avec_copines = map_recue;
            Cellule ma_cellule = new Cellule();
            ma_cellule = la_cellule;
            int parcelle_courrante = numero_de_parcelle;
            if (ma_cellule.get_y() > 1)
            {
                if (map_avec_copines.get_tab_cellules(ma_cellule.get_x(), (ma_cellule.get_y() - 1)).get_est_une_case_jouable() & !(map_avec_copines.get_tab_cellules(ma_cellule.get_x(), (ma_cellule.get_y() - 1)).get_appartient_a_une_parcelle()) & !(ma_cellule.get_frontiere_ouest()))
                {
                    map_avec_copines.get_tab_cellules(ma_cellule.get_x(), (ma_cellule.get_y() - 1)).set_numero_de_parcelle_de_la_cellule(parcelle_courrante); //Maintenant sa variable numparcelledelacellule vaut parcelle courrante
                    map_avec_copines.get_tab_cellules(ma_cellule.get_x(), (ma_cellule.get_y() - 1)).set_appartient_a_une_parcelle(true);//Maintenant elle appartient a une parcelle
                    map_avec_copines.get_parcelle(parcelle_courrante).set_cellule(map_avec_copines.get_tab_cellules(ma_cellule.get_x(), (ma_cellule.get_y() - 1)), cellule_courrante);//Maintenant elle est dans la parcelle courrante
                    cellule_courrante++;
                    map_avec_copines = on_regarde_autour(map_avec_copines, map_avec_copines.get_tab_cellules(ma_cellule.get_x(), (ma_cellule.get_y() - 1)), parcelle_courrante);
                }
            }
            return map_avec_copines;
        }
        //Cette fonction regarde au NORD de la cellule
        static Carte on_regarde_au_nord(Carte map_recue, Cellule la_cellule, int numero_de_parcelle)
        {
            Carte map_avec_copines = new Carte();
            map_avec_copines = map_recue;
            Cellule ma_cellule = new Cellule();
            ma_cellule = la_cellule;
            int parcelle_courrante = numero_de_parcelle;
            if (ma_cellule.get_x() > 1)
            {
                if (map_avec_copines.get_tab_cellules((ma_cellule.get_x() - 1), ma_cellule.get_y()).get_est_une_case_jouable() & !(map_avec_copines.get_tab_cellules((ma_cellule.get_x() - 1), ma_cellule.get_y()).get_appartient_a_une_parcelle()) & !(ma_cellule.get_frontiere_nord()))
                {
                    map_avec_copines.get_tab_cellules((ma_cellule.get_x() - 1), ma_cellule.get_y()).set_numero_de_parcelle_de_la_cellule(parcelle_courrante); //Maintenant sa variable numparcelledelacellule vaut parcelle courrante
                    map_avec_copines.get_tab_cellules((ma_cellule.get_x() - 1), ma_cellule.get_y()).set_appartient_a_une_parcelle(true);//Maintenant elle appartient a une parcelle
                    map_avec_copines.get_parcelle(parcelle_courrante).set_cellule(map_avec_copines.get_tab_cellules((ma_cellule.get_x() - 1), ma_cellule.get_y()), cellule_courrante);//Maintenant elle est dans la parcelle courrante
                    cellule_courrante++;
                    map_avec_copines = on_regarde_autour(map_avec_copines, map_avec_copines.get_tab_cellules((ma_cellule.get_x() - 1), ma_cellule.get_y()), parcelle_courrante);
                }
            }
            return map_avec_copines;
        }

        static Carte on_regarde_autour(Carte map_recue, Cellule la_cellule, int parcelle_courrante)
        {
            Carte map_avec_copines = new Carte();
            Carte map_comparaison = new Carte();
            map_avec_copines = map_recue;
            Cellule ma_cellule = new Cellule();
            ma_cellule = la_cellule;
            int numero_parcelle = parcelle_courrante;
            map_avec_copines = on_regarde_en_est(map_avec_copines, ma_cellule, numero_parcelle);
            map_avec_copines = on_regarde_au_sud(map_avec_copines, ma_cellule, numero_parcelle);
            map_avec_copines = on_regarde_en_ouest(map_avec_copines, ma_cellule, numero_parcelle);
            map_avec_copines = on_regarde_au_nord(map_avec_copines, ma_cellule, numero_parcelle);
            return map_avec_copines;
        }

        static Carte ranger_les_cellules_dans_des_parcelles(Carte map)
        {
            Carte map_avec_les_parcelles = new Carte();
            map_avec_les_parcelles = map;
            int parcelle_courrante = 0;
            for (int x=0; x<10; x++)
            {
                for (int y=0; y<10; y++)
                {
                    if (map_avec_les_parcelles.get_tab_cellules(x, y).get_est_une_case_jouable() && !(map_avec_les_parcelles.get_tab_cellules(x, y).get_appartient_a_une_parcelle()))
                    {
                        map_avec_les_parcelles.get_tab_cellules(x, y).set_numero_de_parcelle_de_la_cellule(parcelle_courrante); 
                        map_avec_les_parcelles.get_tab_cellules(x, y).set_appartient_a_une_parcelle(true);
                        map_avec_les_parcelles.get_parcelle(parcelle_courrante).set_cellule(map_avec_les_parcelles.get_tab_cellules(x, y), 0);
                        map_avec_les_parcelles = on_regarde_autour(map_avec_les_parcelles, map_avec_les_parcelles.get_tab_cellules(x, y),parcelle_courrante);
                        parcelle_courrante++;
                        cellule_courrante = 1;
                    }            
                }
            }
            return map_avec_les_parcelles;
        }
    }
}
