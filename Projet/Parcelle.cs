using System;

namespace Projet
{
    class Parcelle
    {
        protected int numero_de_parcelle;
        protected Cellule[] tab_cellules = new Cellule[6];

        protected int taille;

        public Parcelle() // constructeur obsolete logiquement :) mais on le garde de cot� pour l'instant
        {
            this.numero_de_parcelle = 0;
            for(int i=0; i<6; i++)
            {
                tab_cellules[i] = new Cellule();
            }
        }

        public Parcelle(int numero_de_parcelle) // normalement c'est ce constructeur qu'on utilise :)
        {
            this.numero_de_parcelle = numero_de_parcelle;
            int taille_local=0;
            for (int i = 0; i < 6; i++)
            {
                tab_cellules[i] = new Cellule();
                taille_local++;
            }
            set_taille(taille_local);
            taille_local=0;
        }

        public Cellule get_cellule(int numero_de_cellule)
        {
            return tab_cellules[numero_de_cellule];
        }

        public int get_numero_de_parcelle()
        {
            return this.numero_de_parcelle;
        }

        public void set_numero_de_parcelle(int numero_de_parcelle)
        {
            this.numero_de_parcelle = numero_de_parcelle;
        }
        public void set_cellule(Cellule la_cellule,int numero_de_cellule)
        {
            this.tab_cellules[numero_de_cellule] = la_cellule;
        }

        public void set_taille(int taille){
            this.taille = taille;
        }

        public int get_taille(){
            return this.taille;
        }

    }
    
}
