﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace Sockets
{
    class Program
    {

        public static bool[,,] determination_des_frontieres(){
            Console.WriteLine("Début de la fonction determination_des_frontieres");

            bool[,,] map_frontiere = new bool [10,10,4];
            
            Console.WriteLine("Fin de la fonction determination_des_frontieres");
            return map_frontiere;
        }

        public static string[,] decouper_la_carte(string chaine){
            Console.WriteLine("Ici nous somme au début de la fonction decouper_la_carte");

            string[] ligne = new string[10];
            string[,] map = new string [10,10];
            string[] detail_ligne = new string[10];
            ligne = chaine.Split('|');

            for (int i = 0 ; i<10 ; i++ ){
                detail_ligne = ligne[i].Split(':'); 
                for (int j = 0 ; j<10 ; j++){
                    map[i,j] = detail_ligne[j];
                }
            }

            Console.WriteLine("Fin de la fonction decouper_la_carte");
            return map;
        }

        public static bool[,] determine_quelles_cases_sont_jouables(string[,] map){
            Console.WriteLine("Début de la fonction determine_quelles_cases_sont_jouables");

            bool[,] map_cases_jouables = new bool [10,10]; // True -> case autre que mer / foret 
            for (int indice_ligne = 0; indice_ligne < 10 ; ++indice_ligne){
                for (int indice_colonne = 0; indice_colonne < 10 ; ++indice_colonne){
                    if(int.Parse(map[indice_ligne,indice_colonne]) >= 32){
                        map_cases_jouables[indice_ligne,indice_colonne] = false;
                    }else{
                        map_cases_jouables[indice_ligne,indice_colonne] = true; 
                    }
                }
            }

            Console.WriteLine("Fin de la fonction determine_quelles_cases_sont_jouables");
            return map_cases_jouables;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Ici nous somme au début du main");   Console.WriteLine("");
           /* int reception = 0;
            Encoding unicode = Encoding.Unicode;
            byte[] donnees = new byte[1000];
            Socket socketClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                socketClient.Connect("172.16.0.88", 1212);
                reception = socketClient.Receive(donnees);
            }
            catch(Exception Erreur)
            {
                Console.WriteLine(Erreur.ToString());
                Console.ReadKey();
                return;
            }

       */     string chaine;/*
          
            chaine = Encoding.ASCII.GetString(donnees);
            Console.WriteLine(chaine);
            socketClient.Shutdown(SocketShutdown.Receive);
            socketClient.Close();*/

            // Partie decoupage de la chaine
            string Phatt_Island = "67:69:69:69:69:69:69:69:69:73|74:3:9:7:5:13:3:1:9:74|74:2:8:7:5:13:6:4:12:74|74:6:12:7:9:7:13:3:9:74|74:3:9:11:6:13:7:4:8:74|74:6:12:6:13:11:3:13:14:74|74:7:13:7:13:10:10:3:9:74|74:3:9:11:7:12:14:2:8:74|74:6:12:6:13:7:13:6:12:74|70:69:69:69:69:69:69:69:69:76|";
            string Scabb_Island = "3:9:71:69:65:65:65:65:65:73|2:8:3:9:70:68:64:64:64:72|6:12:2:8:3:9:70:68:64:72|11:11:6:12:6:12:3:9:70:76|10:10:11:11:67:73:6:12:3:9|14:14:10:10:70:76:7:13:6:12|3:9:14:14:11:7:13:3:9:75|2:8:7:13:14:3:9:6:12:78|6:12:3:1:9:6:12:35:33:41|71:77:6:4:12:39:37:36:36:44|";
            string[,] map = new string [10,10];
            chaine = Phatt_Island;
            
            Console.WriteLine("Appel de la fonction decouper_la_carte"); 
            map = decouper_la_carte(chaine); //Ici On appel la fonction decouper_la_carte
            Console.WriteLine("Retour dans le main");   Console.WriteLine("");


            //Partie determination de quels sont les cases jouables et les quelles ne le sont pas 
            bool[,] map_cases_jouables = new bool [10,10]; // True -> case autre que mer / foret 
            
            Console.WriteLine("Appel de la fonction determine_quelles_cases_sont_jouables");
            map_cases_jouables = determine_quelles_cases_sont_jouables(map);
            Console.WriteLine("Retour dans le Main");   Console.WriteLine("");


            //Histoire que la console ne se ferme pas directement
             Console.WriteLine("Ici nous somme à la fin du Main");
            Console.ReadKey();


        }
    }
}
